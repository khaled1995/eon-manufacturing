import Dashboard from './Dashboard';
// import i18next from 'i18next';
// import { authRoles } from 'app/auth';
// import en from './i18n/en';
// import tr from '../i18n/tr';
// import ar from '../i18n/ar';

// i18next.addResourceBundle('en', 'Dashboard', en);
// i18next.addResourceBundle('tr', 'examplePage', tr);
// i18next.addResourceBundle('ar', 'examplePage', ar);

const dashboardConfig = {
	settings: {
		layout: {
			config: {
				
			}
		}
	},
	// auth: authRoles.admin,
	routes: [
		{
			path: '/dashboard',
			component: Dashboard
		}
	]
};

export default dashboardConfig;

/**
 * Lazy load Example
 */
/*
import React from 'react';

const ExampleConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
        {
            path     : '/example',
            component: React.lazy(() => import('./Example'))
        }
    ]
};

export default ExampleConfig;

*/
