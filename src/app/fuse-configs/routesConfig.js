import React from 'react';
import { Redirect } from 'react-router-dom';
import FuseUtils from '@fuse/utils';
import dashboardConfig from 'app/main/dashboard/DashboardConfig';
import login from 'app/main/login/LoginConfig';
import cases from 'app/main/cases/CasesConfig';

const routeConfigs = [dashboardConfig, login, cases];

const routes = [
	...FuseUtils.generateRoutesFromConfigs(routeConfigs),
	{
		path: '/',
		component: () => <Redirect to="/dashboard" />
	}
];

export default routes;
